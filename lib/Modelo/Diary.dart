import 'package:sqlite/DataBase/CRUD.dart';
import 'package:sqlite/DataBase/DBTable.dart';

class Diary extends CRUD{
  int id;
  String type;
  String enterCode;
  Diary({this.id, this.type="", this.enterCode=""}):super(DBTable.DIARY);

  factory Diary.toObject(Map<dynamic, dynamic> data){
    return (data!=null)?Diary(
      id:data['id'],
      type: data['type'],
      enterCode: data['enterCode']
    ):Diary();
  }

  Map<String, dynamic>toMap(){ //convierte un objeto a map
    return{
      'id':this.id,
      'type':this.type,
      'enterCode':this.enterCode
    };
  }

}