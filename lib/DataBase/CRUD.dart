import 'package:flutter/material.dart';
import 'package:sqflite/sqflite.dart';

abstract class CRUD{  //la clase crud es base, el cual debe ser abstracto para no se pueda creear instacias y podamos tener lo sdatos atravez de la herencia
  final String table;

  const CRUD(this.table);

  Future<Database>get database async{
    return await DB().open();
  }
//consultar datos de nuestra base
  query(String sql, [List<dynamic> arguments])async{
    final db=await database;
    return await db.rawQuery(sql,arguments); //sentencia preparada
  }
  update(Map<String,dynamic>data)async{
    final db=await database;
    return await db.update(table, values, where: "id =?", whereArgs:[data["id"]]);
  }


  insert(Map<String,dynamic>data)async{
    final db=await database;
    return await db.insert(table, data);
  }

  delete(int id)async{
    final db= await database;
        return await db.delete(table,where: "id =?", whereArgs:[id]);


  }
}