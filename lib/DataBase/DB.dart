import 'package:flutter/foundation.dart';
import 'package:sqflite/sqflite.dart';
import 'package:path/path.dart';
import 'package:sqlite/DataBase/DBTable.dart';

class DB{
  String name = "DiaryApp";
  int version=1;
  Future<Database>open() async{
    String path = join(await getDatabasesPath(), name); //Join convierte el valor en string y nos permitira concatenar los string
    openDatabase(path,
    version: version,
    onConfigure:onConfigure,
    onCreate: onCreate);
   }
}

onCreate(Database db, int version)async{
DBTable.TABLES.forEach((scrip) async=>await db.execute(scrip));
}



onConfigure(Database db)async{
  await db.execute("PRAGMA foreign_keys = ON")
}